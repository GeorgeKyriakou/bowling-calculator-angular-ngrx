import { Routes, RouterModule } from '@angular/router';

export const APP_ROUTES: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'game' },
  { path: 'game', loadChildren: '../game/game.module#GameModule'}
];
