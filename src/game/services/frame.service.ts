import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';

import { Frame } from '../models/frame.model';

const bowlingApi = 'http://localhost:5000/game';

@Injectable()
export class FrameService {

    constructor( private http: Http ) { }

    calculateFrame(frame: object): Observable<Frame> {
        return this.http.post(`${bowlingApi}/frame`, frame)
          .pipe(catchError(e => Observable.throw(e.json()))) as Observable<Frame>
    }

    resetGame() {
      return this.http.get(`${bowlingApi}/reset`)
        .pipe(catchError(e => Observable.throw(e.json())));
    }
}
