import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';

import { Game } from '../models/game.model';

const bowlingApi = 'http://localhost:5000/history';

@Injectable()
export class HistoryService {

    constructor( private http: Http ) { }

    addGameToHistory(game: Game): Observable<Game[]> {
        return this.http.post(`${bowlingApi}/add`, game)
          .pipe(catchError(e => Observable.throw(e.json()))) as Observable<Game[]>
    }

    loadHistory() {
      return this.http.get(`${bowlingApi}/load`)
        .pipe(catchError(e => Observable.throw(e.json())));
    }

    deleteHistory() {
      return this.http.get(`${bowlingApi}/clear`)
        .pipe(catchError(e => Observable.throw(e.json())));
    }
}
