export interface Frame {
  first: any;
  second: any;
  third?: number;
  total?: number;
}
