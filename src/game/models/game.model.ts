import { Frame } from './frame.model';

export interface Game {
  data: Frame[];
  totalScore: number;
}
