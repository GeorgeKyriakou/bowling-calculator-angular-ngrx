import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import * as fromStore from '../store';
import * as frameActions from '../store/actions/frame.action';
import * as historyActions from '../store/actions/history.action';
import { Game } from '../models/game.model';
import { Frame } from '../models/frame.model';

let first = 0;
let counter = 0;

function validateTypeOfInput(control: FormControl) {
  if (isNaN(parseInt(control.value, 10))) {
    return control.value === null ? null : { err: 1};
  } else {
    return null;
  }
}

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styleUrls: ['./frame.component.css']
})
export class FrameComponent implements OnInit {
  first: number;
  frameForm: FormGroup;
  frames: Frame[];
  totalScore: number;
  invalid: boolean;
  showThirdInput: boolean;
  showHistory: boolean;

  constructor(
    private store: Store<fromStore.ApplicationState>,
    private formBuilder: FormBuilder
  ) {
    this.frameForm = this.formBuilder.group({
      'first' : [ null, [ Validators.min(0),
        Validators.max(10),
        Validators.required,
        validateTypeOfInput
        ]
      ],
      'second': [ null, [ Validators.min(0),
        Validators.max(10),
        validateTypeOfInput
        ]
      ],
      'third' : [ null, [ Validators.min(0),
        Validators.max(10),
        validateTypeOfInput
      ]]
    });
}

ngOnInit() {
  counter = 0;
  this.showThirdInput = false;
  this.showHistory = false;
  this.store.select('game').subscribe( state => {
    this.frames = state['game']['data'];
    this.totalScore = state['game']['totalScore'];
  });
}
addToHistory() {
  const gameToAdd = {
    data: this.frames,
    totalScore: this.totalScore
  };
  this.store.dispatch(new historyActions.AddToHistory(gameToAdd));
  this.resetGame();
}
calculateFrame(frame) {
  this.showThirdInput = false;
  if (counter === 10) {
    this.addToHistory();
    return;
  }
  frame.first = parseInt(frame.first, 10);
  frame.second = frame.second !== null ? parseInt(frame.second, 10) : 0 ;
  frame.third = frame.third !== null ? parseInt(frame.third, 10) : 0;
  first = frame.first;

  if (frame.third === 0 && frame.first + frame.second > 10) {
    this.notValidInput();
  } else {
    frame.counter = counter;
    this.store.dispatch(new frameActions.AddNormal(frame));
    this.frameForm.reset();
    counter++;
  }
}

firstRoundPlayed(e) {
  first = parseInt(e.target.value, 10);
  if (first === 10 && counter === 9) {
    this.showThirdInput = true;
  }
}
notValidInput() {
  this.invalid = true;
  setTimeout(() => {
    this.invalid = false;
  }, 3000);
}
resetGame() {
  counter = 0;
  this.store.dispatch(new frameActions.ResetGame());
}
toggleHistory() {
  this.showHistory = !this.showHistory;
}
}
