import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { Game } from '../models/game.model';

import * as fromStore from '../store';
import * as historyActions from '../store/actions/history.action';

@Component({
  selector: 'app-end-game',
  templateUrl: './end-game.component.html',
  styleUrls: ['./end-game.component.css']
})
export class EndGameComponent {
  @Input() fetchHistory;
  historyOfGames: Game[];

  constructor( private store: Store<fromStore.ApplicationState> ) {
    this.store.select(fromStore.getApplicationState).subscribe(
      state => {
        this.historyOfGames = state['history']['games'];
      });
      this.store.dispatch(new historyActions.LoadHistory());
  }

  clearHistory() {
    this.store.dispatch(new historyActions.DeleteHistory());
  }
}
