import { Frame } from '../../models/frame.model';

import * as frameActions from '../actions/frame.action';

export interface GameState {
  data: Frame[];
  totalScore: number;
  loaded: boolean;
  loading: boolean;
}

export const gameInit = {
  data : [
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null},
    {'first': null, 'second': null, 'third': null, 'total': null}
  ],
  totalScore : 0,
  loaded: false,
  loading: false
};


export function GameReducer(state: GameState = gameInit, action: frameActions.FrameActions ) {
  switch (action.type) {
    case frameActions.ADD_FRAME:
      return {
        ...state,
        loading: true
      };
    case frameActions.ADD_FRAME_SUCCESS:
    const result = action.payload;
    const totalScore = JSON.parse(result['calculatedFrame']['_body']);
    const first = parseInt(result['frame']['first'], 10);
    const second = parseInt(result['frame']['second'], 10);
    const third = parseInt(result['frame']['third'], 10);
    state.data[result['frame']['counter']] = {
      first,
      second,
      third,
      total: first + second + third
    };
      return {
        ...state,
        loading : false,
        loaded: true,
        totalScore: totalScore.score
      };
    case frameActions.ADD_FRAME_FAIL:
      return {
        ...state,
        loading : false,
        loaded: false
      };
    case frameActions.RESET_GAME: {
      return {
        ...state,
        loading: true
      };
    }
    case frameActions.RESET_GAME_SUCCESS: {
      state.data.forEach(frame => {
        frame['first'] = null;
        frame['second'] = null;
        frame['third'] = null;
        frame['total'] = null;
      });
      return {
        ...state,
        totalScore : 0
      };
    }
    case frameActions.RESET_GAME_FAIL: {
      return {
        ...state,
        loading: true,
        loaded: false
      };
    }
    default:
      return state;
  }
}

export const getGameState = (state: GameState) => state.data ;
export const getLoaded = (state: GameState) => state.loaded;
export const getLoading = (state: GameState) => state.loading;

