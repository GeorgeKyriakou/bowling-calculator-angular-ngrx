import { Action, ActionReducerMap, createSelector, createFeatureSelector } from '@ngrx/store';

import { Game } from '../../models/game.model';

import * as historyActions from '../actions/history.action';
export interface HistoryState {
  games: Game[];
  loaded: boolean;
  loading: boolean;
}

export const initialState: HistoryState = {
  games: [],
  loaded: false,
  loading: false
};

export function HistoryReducer(state: HistoryState = initialState, action: historyActions.HistoryActions ): HistoryState {
  let previous;
  switch (action.type) {
    case historyActions.LOAD_HISTORY:
      return {
        ...state,
        loading: true
      };
    case historyActions.LOAD_HISTORY_SUCCESS:
    const response = JSON.parse(action.payload['_body']);
      return {
        ...state,
        loading : false,
        loaded: true,
        games: response
      };
    case historyActions.LOAD_HISTORY_FAIL:
      return {
        ...state,
        loading : false,
        loaded: false
      };
    case historyActions.ADD_TO_HISTORY: {
      return {
        ...state,
        loading: true
      };
    }
    case historyActions.ADD_TO_HISTORY_SUCCESS: {
     previous = action.payload;
      return {
          ...state,
          loading : false,
          loaded: true,
      };
    }
    case historyActions.ADD_TO_HISTORY_FAIL: {
      return {
        ...state,
        loading: true,
        loaded: false
      };
    }
    default:
      return state;
  }
}

export const getHistory = (state: HistoryState) => state.games;
export const getLoading = (state: HistoryState) => state.loading;
export const getLoaded = (state: HistoryState) => state.loaded;
