import {ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as game from './game.reducer';
import * as history from './history.reducer';

export interface ApplicationState {
  game: game.GameState;
  history: history.HistoryState;
}

export const reducers: ActionReducerMap<ApplicationState> = {
  game: game.GameReducer,
  history: history.HistoryReducer,
};

export const getApplicationState = createFeatureSelector<ApplicationState>('game');
