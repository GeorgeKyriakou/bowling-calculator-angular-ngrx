import { Injectable } from "@angular/core";

import { switchMap, map, catchError } from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { Effect, Actions, ofType } from "@ngrx/effects";

import * as frameActions from "../actions/frame.action";
import { FrameService } from "../../services/frame.service";

@Injectable()
export class FrameEffects {
  constructor(private actions$: Actions, private frameService: FrameService) {}

  @Effect()
  sendRound$ = this.actions$.pipe(
    ofType(frameActions.ADD_FRAME),
    map((action: frameActions.AddNormal) => action.payload),
    switchMap(frame => {
      return this.frameService.calculateFrame(frame).pipe(
        map(
          calculatedFrame =>
            new frameActions.AddNormalSuccess({ calculatedFrame, frame })
        ),
        catchError(e => of(new frameActions.AddNormalFail(e)))
      );
    })
  );

  @Effect()
  resetGame$ = this.actions$.pipe(
    ofType(frameActions.RESET_GAME),
    map((action: frameActions.AddNormal) => action.payload),
    switchMap(frame => {
      return this.frameService.resetGame().pipe(
        map(calculatedFrame => new frameActions.ResetGameSuccess()),
        catchError(e => of(new frameActions.AddNormalFail(e)))
      );
    })
  );
}
