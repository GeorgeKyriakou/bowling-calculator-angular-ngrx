import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, async, inject } from '@angular/core/testing';

import { Actions } from '@ngrx/effects';

import { hot, cold } from 'jasmine-marbles';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

import { HistoryService } from '../../services/history.service';
import * as fromEffects from './history.effects';
import * as fromActions from '../actions/history.action';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

describe('Frame Effects', () => {
  let actions$: TestActions;
  let service: HistoryService;
  let effects: fromEffects.HistoryEffects;

  const payload = [{
    data: [
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 },
      { first: 1, second: 2, third: 0, counter: 0 }
    ],
    totalScore: 30
  }];
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        HistoryService,
        HttpClientTestingModule,
        fromEffects.HistoryEffects,
        { provide: Actions, useFactory: getActions },
      ],
    });

    actions$ = TestBed.get(Actions);
    service = TestBed.get(HistoryService);
    effects = TestBed.get(fromEffects.HistoryEffects);

    spyOn(service, 'loadHistory').and.returnValue(of(payload));
  });

  describe('loadHistory$', () => {

    it('should return a array of games from AddNormalSuccess',
    async(inject([HttpTestingController, HistoryService],
      (httpClient: HttpTestingController, apiService: HistoryService) => {
        const action = new fromActions.LoadHistory();
        const completion = new fromActions.LoadHistorySuccess(payload);

        actions$.stream = hot('-a', { a: action });
        const expected = cold('-b', { b: completion });

        expect(effects.loadHistory$).toBeObservable(expected);
      })));
    });
  });
