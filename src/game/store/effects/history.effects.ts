import { Injectable } from "@angular/core";

import { switchMap, map, catchError } from "rxjs/operators";
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { Effect, Actions, ofType } from "@ngrx/effects";

import * as historyActions from "../actions/history.action";
import { HistoryService } from "../../services/history.service";

@Injectable()
export class HistoryEffects {
  constructor(
    private actions$: Actions,
    private historyService: HistoryService
  ) {}

  @Effect()
  loadHistory$ = this.actions$.pipe(
    ofType(historyActions.LOAD_HISTORY),
    switchMap(() => {
      return this.historyService.loadHistory().pipe(
        map(games => new historyActions.LoadHistorySuccess(games)),
        catchError(e => of(new historyActions.LoadHistoryFail(e)))
      );
    })
  );

  @Effect()
  addToHistory$ = this.actions$.pipe(
    ofType(historyActions.ADD_TO_HISTORY),
    map((action: historyActions.AddToHistory) => action.payload),
    switchMap(game => {
      return this.historyService.addGameToHistory(game).pipe(
        map(history => new historyActions.AddToHistorySuccess(history)),
        catchError(e => of(new historyActions.AddToHistoryFail(e)))
      );
    })
  );

  @Effect()
  clearHistory$ = this.actions$.pipe(
    ofType(historyActions.DELETE_HISTORY),
    switchMap(() => {
      return this.historyService.deleteHistory().pipe(
        map(games => new historyActions.DeleteHistorySuccess()),
        catchError(e => of(new historyActions.DeleteHistoryFail(e)))
      );
    })
  );
}
