import { FrameEffects } from './frame.effects';
import { HistoryEffects } from './history.effects';

export const effects: any[] = [ FrameEffects, HistoryEffects ];

export * from './frame.effects';
export * from './history.effects';
