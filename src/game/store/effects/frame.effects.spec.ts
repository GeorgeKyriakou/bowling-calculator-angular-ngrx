import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpModule } from '@angular/http';

import { TestBed, async, inject } from '@angular/core/testing';

import { Actions } from '@ngrx/effects';

import { hot, cold } from 'jasmine-marbles';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';

import { FrameService } from '../../services/frame.service';
import * as fromEffects from './frame.effects';
import * as fromActions from '../actions/frame.action';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

describe('Frame Effects', () => {
  let actions$: TestActions;
  let service: FrameService;
  let effects: fromEffects.FrameEffects;

  const score = { score: 8 };
  const frame = { first: 4, second: 4, third: 0, counter: 0 };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        FrameService,
        HttpModule,
        fromEffects.FrameEffects,
        { provide: Actions, useFactory: getActions },
      ],
    });

    actions$ = TestBed.get(Actions);
    service = TestBed.get(FrameService);
    effects = TestBed.get(fromEffects.FrameEffects);

    spyOn(service, 'calculateFrame').and.returnValue(of(score));
  });

  describe('sendRound$', () => {

    it('should return a score count from AddNormalSuccess',
     async(inject([HttpTestingController, FrameService],
    (httpClient: HttpTestingController, apiService: FrameService) => {
      const action = new fromActions.AddNormal(frame);
      const completion = new fromActions.AddNormalSuccess(score);

      actions$.stream = hot('-a', { a: action });
      const expected = cold('-b', { b: completion });

      expect(effects.sendRound$).toBeObservable(expected);
   })));
  });
});
