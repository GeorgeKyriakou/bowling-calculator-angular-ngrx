import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Game } from '../../models/game.model';

export const LOAD_HISTORY = '[History] Load ';
export const LOAD_HISTORY_SUCCESS = '[History] Load: Success';
export const LOAD_HISTORY_FAIL = '[History] Load: Fail';

export class LoadHistory implements Action {
  readonly type = LOAD_HISTORY;
}

export class LoadHistorySuccess implements Action {

  readonly type = LOAD_HISTORY_SUCCESS;
  constructor( public payload: Game[]) { }

}

export class LoadHistoryFail implements Action {
  readonly type = LOAD_HISTORY_FAIL;
  constructor( public payload: any) { }

}

export const ADD_TO_HISTORY = '[History] Add';
export const ADD_TO_HISTORY_SUCCESS = '[History] Add: Success';
export const ADD_TO_HISTORY_FAIL = '[History] Add: Fail';

export class AddToHistory implements Action {
  readonly type = ADD_TO_HISTORY;
  constructor( public payload: Game) { }

}

export class AddToHistorySuccess implements Action {
  readonly type = ADD_TO_HISTORY_SUCCESS;
  constructor( public payload: Game[]) { }

}

export class AddToHistoryFail implements Action {
  readonly type = ADD_TO_HISTORY_FAIL;
  constructor( public payload: any) { }

}

export const DELETE_HISTORY = '[History] Delete';
export const DELETE_HISTORY_SUCCESS = '[History] Delete: Success';
export const DELETE_HISTORY_FAIL = '[History] Delete: Fail';

export class DeleteHistory implements Action {
  readonly type = DELETE_HISTORY;
}

export class DeleteHistorySuccess implements Action {
  readonly type = DELETE_HISTORY_SUCCESS;

}

export class DeleteHistoryFail implements Action {
  readonly type = DELETE_HISTORY_FAIL;
  constructor( public payload: any) { }

}

export type HistoryActions =
    LoadHistory
  | LoadHistorySuccess
  | LoadHistoryFail
  | AddToHistory
  | AddToHistorySuccess
  | AddToHistoryFail;
