import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Frame } from '../../models/frame.model';

export const ADD_FRAME = '[Frame] Add ';
export const ADD_FRAME_SUCCESS = '[Frame] Add: Success';
export const ADD_FRAME_FAIL = '[Frame] Add: Fail';

export class AddNormal implements Action {

  readonly type = ADD_FRAME;
  constructor( public payload: object) { }

}

export class AddNormalSuccess implements Action {

  readonly type = ADD_FRAME_SUCCESS;
  constructor( public payload: object) { }

}

export class AddNormalFail implements Action {

  readonly type = ADD_FRAME_FAIL;
  constructor( public payload: object) { }

}

export const RESET_GAME = '[Game] Reset';
export const RESET_GAME_SUCCESS = '[Game] Reset: Success';
export const RESET_GAME_FAIL = '[Game] Reset: Fail';

export class ResetGame implements Action {
  readonly type = RESET_GAME;
}

export class ResetGameSuccess implements Action {
  readonly type = RESET_GAME_SUCCESS;
}

export class ResetGameFail implements Action {
  readonly type = RESET_GAME_FAIL;
  constructor( public payload: object) { }

}

export type FrameActions =
    AddNormal
  | AddNormalFail
  | AddNormalSuccess
  | ResetGame
  | ResetGameFail
  | ResetGameSuccess;
