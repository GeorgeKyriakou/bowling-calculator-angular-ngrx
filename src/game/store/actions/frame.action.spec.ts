import * as fromFrame from './frame.action';

  describe('Add new frame Actions', () => {
    describe('Add Frame', () => {
      it('should create an action', () => {
        const payload = {
          first: 1,
          second: 2,
          third: 0,
          counter: 0
        };
        const action = new fromFrame.AddNormal(payload);
        expect({ ...action }).toEqual({
          type: fromFrame.ADD_FRAME,
          payload,
        });
      });
    });

    describe('Add frame Fail', () => {
      it('should create an action', () => {
        const payload = { message: 'Update Error' };
        const action = new fromFrame.AddNormalFail(payload);
        expect({ ...action }).toEqual({
          type: fromFrame.ADD_FRAME_FAIL,
          payload,
        });
      });
    });
    describe('Add frame Success', () => {
      it('should create an action', () => {
        const payload = [
          {
            first: 1,
            second: 2,
            third: 0,
            counter: 0
          }
        ];
        const action = new fromFrame.AddNormalSuccess(payload);
        expect({ ...action }).toEqual({
          type: fromFrame.ADD_FRAME_SUCCESS,
          payload,
        });
      });
    });
  });

  describe('Reset Game Actions', () => {
    describe('Reset Game', () => {
      it('should create an action', () => {
        const action = new fromFrame.ResetGame();
        expect({ ...action }).toEqual({
          type: fromFrame.RESET_GAME,
        });
      });
    });
    describe('Reset Game Fail', () => {
      it('should create an action', () => {
        const payload = { message: 'Update Error' };
        const action = new fromFrame.ResetGameFail(payload);
        expect({ ...action }).toEqual({
          type: fromFrame.RESET_GAME_FAIL,
          payload
        });
      });
    });
    describe('Reset Game Success', () => {
      it('should create an action', () => {
        const action = new fromFrame.ResetGameSuccess();
        expect({ ...action }).toEqual({
          type: fromFrame.RESET_GAME_SUCCESS,
        });
      });
    });
  });
