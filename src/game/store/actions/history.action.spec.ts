import * as fromHistory from './history.action';

  describe('History Actions', () => {
    describe('Load', () => {
      describe('Load History', () => {
        it('should create an action', () => {
          const action = new fromHistory.LoadHistory();
          expect({ ...action }).toEqual({
            type: fromHistory.LOAD_HISTORY,
          });
        });
      });

      describe('Load History Fail', () => {
        it('should create an action', () => {
          const payload = { message: 'Update Error' };
          const action = new fromHistory.LoadHistoryFail(payload);
          expect({ ...action }).toEqual({
            type: fromHistory.LOAD_HISTORY_FAIL,
            payload,
          });
        });
      });
      describe('Load History Success', () => {
        it('should create an action', () => {
          const payload = [{
          data: [
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 }
          ],
        totalScore: 30
      }];
          const action = new fromHistory.LoadHistorySuccess(payload);
          expect({ ...action }).toEqual({
            type: fromHistory.LOAD_HISTORY_SUCCESS,
            payload,
          });
        });
      });
    });

    describe('Add', () => {
      describe('Add to History', () => {
        it('should create an action', () => {
          const payload = {
          data: [
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 },
            { first: 1, second: 2, third: 0, counter: 0 }
          ],
        totalScore: 30
        };
          const action = new fromHistory.AddToHistory(payload);
          expect({ ...action }).toEqual({
            type: fromHistory.ADD_TO_HISTORY,
            payload,
          });
        });
      });
    describe('Add to History Success', () => {
      it('should create an action', () => {
        const payload = [{
        data: [
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 },
          { first: 1, second: 2, third: 0, counter: 0 }
        ],
      totalScore: 30
      }];
        const action = new fromHistory.AddToHistorySuccess(payload);
        expect({ ...action }).toEqual({
          type: fromHistory.ADD_TO_HISTORY_SUCCESS,
          payload,
        });
      });
    });

    describe('Add to History Fail', () => {
      it('should create an action', () => {
        const payload = { message: 'Update Error' };
        const action = new fromHistory.AddToHistoryFail(payload);
        expect({ ...action }).toEqual({
          type: fromHistory.ADD_TO_HISTORY_FAIL,
          payload,
        });
      });
    });
  });

  describe('Clear', () => {
    describe('Clear History', () => {
      it('should create an action', () => {
        const action = new fromHistory.DeleteHistory();
        expect({ ...action }).toEqual({
          type: fromHistory.DELETE_HISTORY,
        });
      });
    });
  describe('Clear History Success', () => {
    it('should create an action', () => {
      const action = new fromHistory.DeleteHistorySuccess();
      expect({ ...action }).toEqual({
        type: fromHistory.DELETE_HISTORY_SUCCESS,
      });
    });
  });

  describe('Clear History Fail', () => {
    it('should create an action', () => {
      const payload = { message: 'Update Error' };
      const action = new fromHistory.DeleteHistoryFail(payload);
      expect({ ...action }).toEqual({
        type: fromHistory.DELETE_HISTORY_FAIL,
        payload,
      });
    });
  });
});
});
