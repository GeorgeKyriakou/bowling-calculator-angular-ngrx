import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { GAME_ROUTES } from './game.routes';

import { reducers, effects } from './store';

import { FrameEffects } from './store/effects/frame.effects';
import { HistoryEffects } from './store/effects/history.effects';

import { FrameComponent } from './frame/frame.component';
import { EndGameComponent } from './end-game/end-game.component';

import { FrameService } from './services/frame.service';
import { HistoryService } from './services/history.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forChild(GAME_ROUTES),
    StoreModule.forFeature('game', reducers),
    EffectsModule.forFeature( effects )
  ],
  providers: [ FrameService, HistoryService ],
  declarations: [ FrameComponent, EndGameComponent ],
  exports: [ ]
})
export class GameModule {}
