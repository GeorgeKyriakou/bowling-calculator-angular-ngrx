import { Routes } from '@angular/router';
import { FrameComponent } from './frame/frame.component';

export const GAME_ROUTES: Routes = [
  { path: '', component: FrameComponent }
];
